package com.tencent.rtc;

import android.content.Context;
import android.content.Intent;

public class FloatActionController {
    public static void startFloatServer(Context context) {
        Intent intent = new Intent(context, FloatService.class);
        context.startService(intent);
    }
}
