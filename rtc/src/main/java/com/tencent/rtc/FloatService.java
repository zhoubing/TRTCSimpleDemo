package com.tencent.rtc;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.Nullable;

import com.tencent.rtmp.ui.TXCloudVideoView;
import com.tencent.trtc.TRTCCloud;
import com.tencent.trtc.TRTCCloudListener;

import java.util.ArrayList;
import java.util.List;

public class FloatService extends Service {
    private List<TXCloudVideoView> mRemoteViewList = new ArrayList<>();             // 远端用户Id列表
    private List<String> mRemoteUidList = new ArrayList<>();            // 远端用户Id列表

    private TRTCCloud mTRTCCloud;
    private TXCloudVideoView videoView;
    @Override
    public void onCreate() {
        super.onCreate();
        mTRTCCloud = TRTCCloud.sharedInstance(getApplicationContext());
        mTRTCCloud.setListener(new TRTCCloudListener() {
            @Override
            public void onError(int i, String s, Bundle bundle) {
                super.onError(i, s, bundle);
            }

            @Override
            public void onUserVideoAvailable(String userId, boolean available) {
                int index = mRemoteUidList.indexOf(userId);
                if (available) {
                    if (index != -1) { //如果mRemoteUidList有，就不重复添加
                        return;
                    }
                    mRemoteUidList.add(userId);
                    refreshRemoteVideoViews();
                } else {
                    if (index == -1) { //如果mRemoteUidList没有，说明已关闭画面
                        return;
                    }
                    /// 关闭用户userId的视频画面
                    mTRTCCloud.stopRemoteView(userId);
                    mRemoteUidList.remove(index);
                    refreshRemoteVideoViews();
                }
            }
        });
        videoView = new TXCloudVideoView(this);
        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopSelf();
                Intent intent = new Intent(FloatService.this, RTCActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mRemoteViewList.add(videoView);
        WindowManager.LayoutParams windowManagerParams = new WindowManager.LayoutParams();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            windowManagerParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else if (Build.VERSION.SDK_INT >= 24) {
            windowManagerParams.type = WindowManager.LayoutParams.TYPE_PHONE;
        } else {
            windowManagerParams.type = WindowManager.LayoutParams.TYPE_TOAST;
        }
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        int screenWidth = dm.widthPixels;
        int heightPixels = dm.heightPixels;
        windowManagerParams.width = 160;
        windowManagerParams.height = 230;
        windowManagerParams.x = screenWidth;
        windowManagerParams.y = heightPixels;
        windowManagerParams.gravity = Gravity.START | Gravity.TOP;
        videoView.setLayoutParams(windowManagerParams);

        windowManager.getDefaultDisplay().getMetrics(dm);
        windowManager.addView(videoView, windowManagerParams);

        refreshRemoteVideoViews();
    }

    private void refreshRemoteVideoViews() {
        for (int i = 0; i < mRemoteViewList.size(); i++) {
            if (i < RTCActivity.mRemoteUidList.size()) {
                String remoteUid = RTCActivity.mRemoteUidList.get(i);
                mRemoteViewList.get(i).setVisibility(View.VISIBLE);
                // 开始显示用户userId的视频画面
                mTRTCCloud.startRemoteView(remoteUid, mRemoteViewList.get(i));
            } else {
                mRemoteViewList.get(i).setVisibility(View.GONE);
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        windowManager.removeView(videoView);
    }
}
